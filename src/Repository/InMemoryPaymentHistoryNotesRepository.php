<?php

declare(strict_types=1);

namespace App\Repository;

use App\Domain\Entity\PaymentHistoryNote;
use App\Domain\Repositories\PaymentHistoryNotesRepositoryInterface;
use App\Domain\ValueObjects\Id;
use App\Domain\ValueObjects\OperationType;
use DateTime;

/**
 * Class InMemoryPaymentHistoryNotesRepository
 *
 * @package App\Repository
 */
class InMemoryPaymentHistoryNotesRepository implements PaymentHistoryNotesRepositoryInterface
{
    /**
     * @var array
     */
    private array $data = [];

    /**
     * @param  Id            $userId
     * @param  OperationType $type
     * @param  DateTime      $date
     *
     * @return iterable
     */
    public function getSameWeek(Id $userId, OperationType $type, DateTime $date): iterable
    {
        if (!isset($this->data[(string) $userId])) {
            return [];
        }

        return array_filter(
            $this->data[(string) $userId],
            function (PaymentHistoryNote $historyNote) use ($date, $type): bool {

                if (!$historyNote->getType()->same($type)) {
                    return false;
                }

                $nextMonday = new DateTime("{$historyNote->getDate()->format('Y-m-d')} next Monday");
                $previousMonday = new DateTime("{$historyNote->getDate()->format('Y-m-d')} Monday ago");
                $timeToCheckingDate = $previousMonday->diff($date);
                $timeToNextWeek = $previousMonday->diff($nextMonday);

                return $timeToCheckingDate->days < $timeToNextWeek->days;
            }
        );
    }

    /**
     * @param  PaymentHistoryNote $withdrawHistoryNote
     */
    public function save(PaymentHistoryNote $withdrawHistoryNote): void
    {
        if (!isset($this->data[(string) $withdrawHistoryNote->getUserId()])) {
            $this->data[(string) $withdrawHistoryNote->getUserId()] = [];
        }

        $this->data[(string) $withdrawHistoryNote->getUserId()][] = $withdrawHistoryNote;
    }
}