<?php

declare(strict_types=1);

namespace App\Domain\ValueObjects;

/**
 * Class Id
 *
 * @package App\Domain\ValueObjects
 */
class Id
{
    /**
     * @var ?int
     */
    private ?int $value;

    /**
     * Id constructor.
     *
     * @param  int|string|null $value
     */
    public function __construct(int $value = null)
    {
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->value;
    }
}