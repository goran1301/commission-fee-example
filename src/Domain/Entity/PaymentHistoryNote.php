<?php

declare(strict_types=1);

namespace App\Domain\Entity;

use App\Domain\Exception\ValueObjects\CurrencyIsNotConfiguredException;
use App\Domain\ValueObjects\Id;
use App\Domain\ValueObjects\Money;
use App\Domain\ValueObjects\OperationType;
use DateTime;

/**
 * Class PaymentHistoryNote
 *
 * @package App\Domain\Entities
 */
class PaymentHistoryNote
{
    /**
     * @var ?int
     */
    protected ?int $id;

    /**
     * @var int|null
     */
    protected ?int $userId;

    /**
     * @var DateTime
     */
    protected DateTime $date;

    /**
     * @var int
     */
    protected int $amount;

    /**
     * @var int
     */
    protected int $type;

    /**
     * @var string
     */
    protected string $currency;

    /**
     * @var int
     */
    protected int $euroAmount;

    /**
     * PaymentHistoryNote constructor.
     *
     * @param  Money         $amount
     * @param  OperationType $type
     * @param  Id            $userId
     * @param  Money         $euroAmount
     * @param  DateTime|null $date
     */
    public function __construct(
        Money $amount,
        OperationType $type,
        Id $userId,
        Money $euroAmount,
        ?DateTime $date = null
    ) {
        $this->userId = intval((string) $userId);
        $this->type = $type->toInt();
        $this->amount = $amount->getInteger();
        $this->currency = $amount->getCurrency();
        $this->date = $date ?? new DateTime();
        $this->euroAmount = $euroAmount->getInteger();
    }

    /**
     * @return Id
     */
    public function getId(): Id
    {
        return new Id((int) $this->id);
    }

    /**
     * @return DateTime
     */
    public function getDate(): DateTime
    {
        return $this->date;
    }

    /**
     * @return Money
     * @throws CurrencyIsNotConfiguredException
     */
    public function getAmount(): Money
    {
        return Money::fromInteger($this->amount, $this->currency);
    }

    /**
     * @return OperationType
     */
    public function getType(): OperationType
    {
        return new OperationType($this->type);
    }

    /**
     * @return int|string|null
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @return Money
     * @throws CurrencyIsNotConfiguredException
     */
    public function getEuroAmount(): Money
    {
        return Money::fromInteger($this->euroAmount, Money::EUR);
    }

}