<?php

declare(strict_types=1);

namespace App\Domain\CommandHandlers;

use App\Domain\Commands\CommissionFeeCommand;
use App\Domain\ValueObjects\Money;

/**
 * Class CommissionFeeCommandHandler
 *
 * @package App\Domain\CommandHandlers
 */
interface CommissionFeeCommandHandler
{
    /**
     * @param  CommissionFeeCommand $command
     *
     * @return Money
     */
    public function handle(CommissionFeeCommand $command): Money;

    /**
     * @param  CommissionFeeCommand $command
     *
     * @return bool
     */
    public function isResponsible(CommissionFeeCommand $command): bool;
}