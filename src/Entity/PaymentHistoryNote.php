<?php

declare(strict_types=1);

namespace App\Entity;

use App\Domain\Entity\PaymentHistoryNote as DomainPaymentHistoryNote;
use DateTime;

/**
 * Class PaymentHistoryNote
 */
class PaymentHistoryNote extends DomainPaymentHistoryNote
{
    /**
     * @var int|null
     */
    protected ?int $id;

    /**
     *
     * @var int|null
     */
    protected ?int $userId;

    /**
     * @var DateTime
     */
    protected DateTime $date;

    /**
     * @var int
     */
    protected int $amount;

    /**
     * @var int
     */
    protected int $type;

    /**
     * @var string
     */
    protected string $currency;

    /**
     * @var int
     */
    protected int $euroAmount;
}