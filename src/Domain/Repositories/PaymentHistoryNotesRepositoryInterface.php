<?php

declare(strict_types=1);

namespace App\Domain\Repositories;

use App\Domain\Entity\PaymentHistoryNote;
use App\Domain\ValueObjects\Id;
use App\Domain\ValueObjects\OperationType;
use DateTime;

/**
 * Interface WithdrawHistoryNotesRepositoryInterface
 *
 * @package App\Domain\Repositories
 */
interface PaymentHistoryNotesRepositoryInterface
{
    /**
     * @param  Id            $userId
     * @param  OperationType $type
     * @param  DateTime      $date
     *
     * @return PaymentHistoryNote[]
     */
    public function getSameWeek(Id $userId, OperationType $type, DateTime $date): iterable;

    /**
     * @param  PaymentHistoryNote $withdrawHistoryNote
     */
    public function save(PaymentHistoryNote $withdrawHistoryNote): void;
}