<?php

declare(strict_types=1);

namespace App\Domain\ValueObjects;

use App\Domain\Exception\ValueObjects\UserTypeIsNotConfigured;

/**
 * Class UserType
 *
 * @package App\Domain\ValueObjects
 */
class UserType
{
    public const TYPE_PRIVATE = 0;

    public const TYPE_BUSINESS = 1;

    /**
     * @var int[]
     */
    private array $types = [
        self::TYPE_PRIVATE,
        self::TYPE_BUSINESS
    ];

    /**
     * @var int
     */
    private int $type;

    /**
     * UserType constructor.
     *
     * @param  int $type
     *
     * @throws UserTypeIsNotConfigured
     */
    public function __construct(int $type)
    {
        if (!in_array($type, $this->types)) {
            throw new UserTypeIsNotConfigured($type);
        }
        $this->type = $type;
    }

    /**
     * @return bool
     */
    public function isPrivate(): bool
    {
        return $this->type === self::TYPE_PRIVATE;
    }

    /**
     * @return bool
     */
    public function isBusiness(): bool
    {
        return $this->type === self::TYPE_BUSINESS;
    }
}