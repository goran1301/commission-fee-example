<?php

namespace App\Services;

use DomainException;
use Generator;

/**
 * Class CsvReader
 *
 * @package App\Services
 */
class CsvReader
{
    /**
     * @param  string $path
     *
     * @param  string $delimiter
     * @param  int    $length
     *
     * @return Generator
     */
    public function read(string $path, string $delimiter = ',', int $length = 0)
    {
        if (!file_exists($path)) {
            throw new DomainException("file $path not exists");
        }

        if (($handle = fopen($path, "r")) !== FALSE) {

            if ($handle === false) {
                throw new DomainException("file $path is not readable");
            }

            while (!feof($handle)) {
                yield fgetcsv($handle, $length, $delimiter);
            }
        }
    }
}