<?php

namespace App\Domain\Calculations;

use App\Domain\Entity\PaymentHistoryNote;
use App\Domain\Exception\ValueObjects\CurrencyIsNotConfiguredException;
use App\Domain\Exception\ValueObjects\DifferentCurrenciesException;
use App\Domain\ValueObjects\Money;

/**
 * Class PrivateWithdrawCommissionFeeFeeLimit
 *
 * @package App\Domain\Calculations
 */
class PrivateWithdrawCommissionFeeFeeLimit
{
    /**
     * @var string
     */
    private string $limitSize;

    /**
     * @var int
     */
    private int $operationsFreeLimit;

    /**
     * @var string
     */
    private string $basicCurrency;

    /**
     * PrivateWithdrawCommissionFeeFeeLimit constructor.
     *
     * @param  string $limitSize
     * @param  int    $operationsFreeLimit
     * @param  string $basicCurrency
     */
    public function __construct(string $limitSize, int $operationsFreeLimit, string $basicCurrency)
    {
        $this->limitSize = $limitSize;
        $this->operationsFreeLimit = $operationsFreeLimit;
        $this->basicCurrency = $basicCurrency;
    }

    /**
     * @param  PaymentHistoryNote[] $weeklyWithdrawHistory
     *
     * @return Money
     * @throws CurrencyIsNotConfiguredException
     * @throws DifferentCurrenciesException
     */
    public function calculate(iterable $weeklyWithdrawHistory): Money
    {
        $limit = new Money($this->limitSize, $this->basicCurrency);
        $operations = 0;
        foreach ($weeklyWithdrawHistory as $historyNote) {
            $operations++;
            $limit = $limit->sub($historyNote->getEuroAmount());

            if (!$limit->isPositive() || $operations > $this->operationsFreeLimit) {
                $limit = new Money('0', $this->basicCurrency);

                break;
            }
        }

        return $limit;
    }
}