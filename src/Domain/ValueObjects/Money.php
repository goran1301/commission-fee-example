<?php

declare(strict_types=1);

namespace App\Domain\ValueObjects;

use App\Domain\Exception\ValueObjects\CurrencyIsNotConfiguredException;
use App\Domain\Exception\ValueObjects\DifferentCurrenciesException;

/**
 * Class Money
 *
 * @package App\Domain\ValueObjects
 */
class Money
{
    public const EUR = 'EUR';

    public const USD = 'USD';

    public const JPY = 'JPY';

    private int $amount;

    private string $currency;

    private ?string $decimalAmount = null;

    private static array $currencyConfig = [
        self::USD => 2,
        self::EUR => 2,
        self::JPY => 0,
    ];

    /**
     * Money constructor.
     *
     * @param  string $value
     * @param  string $currency
     *
     * @throws CurrencyIsNotConfiguredException
     */
    public function __construct(string $value, string $currency)
    {
        if (!in_array($currency, array_keys(self::$currencyConfig))) {
            throw new CurrencyIsNotConfiguredException($currency);
        }

        $this->currency = $currency;
        $this->amount = (int) round($value * $this->decimalDivisor(), self::$currencyConfig[$currency]);
    }

    /**
     * @param  Money $money
     *
     * @throws DifferentCurrenciesException
     */
    private function checkSameCurrency(self $money): void
    {
        if (!$this->sameCurrency($money)) {
            throw new DifferentCurrenciesException($this->currency, $money->currency);
        }
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        if ($this->decimalAmount === null) {
            $this->decimalAmount = number_format(
                round(
                    $this->amount / $this->decimalDivisor(),
                    self::$currencyConfig[$this->currency]
                )
                , self::$currencyConfig[$this->currency], '.', ''
            );
        }

        return $this->decimalAmount;
    }

    /**
     * @return string
     */
    protected function decimalDivisor(): string
    {
        return bcpow('10', (string) self::$currencyConfig[$this->currency]);
    }

    /**
     * @param  string $multiplier
     *
     * @return $this
     * @throws CurrencyIsNotConfiguredException
     */
    public function multiply(string $multiplier): self
    {
        return self::fromInteger(
            (int) ceil($this->amount * $multiplier),
            $this->currency
        );
    }

    /**
     * @param  string $divisor
     *
     * @return $this
     * @throws CurrencyIsNotConfiguredException
     */
    public function div(string $divisor): self
    {
        return self::fromInteger(
            (int) $this->amount / $divisor,
            $this->currency
        );
    }

    /**
     * @param  Money $money
     *
     * @return $this
     * @throws CurrencyIsNotConfiguredException
     * @throws DifferentCurrenciesException
     */
    public function sub(self $money): self
    {
        $this->checkSameCurrency($money);

        return self::fromInteger($this->amount - $money->amount, $this->currency);
    }

    /**
     * @param  Money $money
     * @param  bool  $strictly
     *
     * @return bool
     * @throws DifferentCurrenciesException
     */
    public function graterThen(self $money, bool $strictly = false): bool
    {
        $this->checkSameCurrency($money);
        if ($strictly) {
            return $this->amount > $money->amount;
        }

        return $this->amount >= $money->amount;
    }

    /**
     * @param  Money $money
     *
     * @return bool
     */
    public function sameCurrency(Money $money): bool
    {
        return $this->currency === $money->currency;
    }

    /**
     * @return bool
     */
    public function isPositive(): bool
    {
        return $this->amount >= 0;
    }

    /**
     * @return int
     */
    public function getInteger(): int
    {
        return $this->amount;
    }

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }

    /**
     * @param  string $currency
     *
     * @return bool
     */
    public function currencyIs(string $currency): bool
    {
        return $this->currency === $currency;
    }

    /**
     * @param  int    $amount
     * @param  string $currency
     *
     * @return static
     * @throws CurrencyIsNotConfiguredException
     */
    public static function fromInteger(int $amount, string $currency): self
    {
        return new self(
            (string) bcdiv(
                (string)$amount, (string)bcpow('10', (string) self::$currencyConfig[$currency]),
                self::$currencyConfig[$currency]
            ),
            $currency
        );
    }

    /**
     * @param  CurrencyRate $rate
     *
     * @return $this
     * @throws CurrencyIsNotConfiguredException
     */
    public function toCurrency(CurrencyRate $rate): self
    {
        if ($rate->isInvert()) {
            return new self(
                (string) ceil(($this->amount / $this->decimalDivisor()) * $rate->getValue()),
                $rate->getTo()
            );
        }
        return new self(
            (string) ceil($this->amount / $this->decimalDivisor() / $rate->getValue()),
            $rate->getTo()
        );
    }

    /**
     * @return bool
     */
    public function isZero(): bool
    {
        return $this->amount === 0;
    }
}