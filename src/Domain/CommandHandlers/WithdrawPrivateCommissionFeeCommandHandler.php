<?php

declare(strict_types=1);

namespace App\Domain\CommandHandlers;

use App\Domain\Calculations\PrivateWithdrawCommissionFeeFeeLimit;
use App\Domain\Commands\CommissionFeeCommand;
use App\Domain\Exception\ValueObjects\CurrencyIsNotConfiguredException;
use App\Domain\Exception\ValueObjects\DifferentCurrenciesException;
use App\Domain\Repositories\PaymentHistoryNotesRepositoryInterface;
use App\Domain\Services\RatesConverterInterface;
use App\Domain\ValueObjects\Money;

/**
 * Class WithdrawPrivateCommissionFeeCommandHandler
 *
 * @package App\Domain\CommandHandlers
 */
class WithdrawPrivateCommissionFeeCommandHandler implements CommissionFeeCommandHandler
{
    /**
     * @var RatesConverterInterface
     */
    private RatesConverterInterface $ratesConverter;

    /**
     * @var PaymentHistoryNotesRepositoryInterface
     */
    private PaymentHistoryNotesRepositoryInterface $paymentHistoryRepository;

    /**
     * @var PrivateWithdrawCommissionFeeFeeLimit
     */
    private PrivateWithdrawCommissionFeeFeeLimit $limitCalculation;

    /**
     * @var string
     */
    private string $commissionPercent;

    /**
     * WithdrawPrivateCommissionFeeCommandHandler constructor.
     *
     * @param  RatesConverterInterface                $ratesConverter
     * @param  PaymentHistoryNotesRepositoryInterface $historyNotesRepository
     * @param  PrivateWithdrawCommissionFeeFeeLimit   $limitCalculation
     * @param  string                                 $commissionPercent
     */
    public function __construct(
        RatesConverterInterface $ratesConverter,
        PaymentHistoryNotesRepositoryInterface $historyNotesRepository,
        PrivateWithdrawCommissionFeeFeeLimit $limitCalculation,
        string $commissionPercent
    ) {
        $this->ratesConverter = $ratesConverter;
        $this->paymentHistoryRepository = $historyNotesRepository;
        $this->limitCalculation = $limitCalculation;
        $this->commissionPercent = $commissionPercent;
    }

    /**
     * @param  CommissionFeeCommand $command
     *
     * @return Money
     * @throws CurrencyIsNotConfiguredException
     * @throws DifferentCurrenciesException
     */
    public function handle(CommissionFeeCommand $command): Money
    {
        $withdraws = $this->paymentHistoryRepository->getSameWeek(
            $command->getUserIdentifier(),
            $command->getOperationType(),
            $command->getDate()
        );

        $limit = $this->limitCalculation->calculate($withdraws);
        $withdraw = $command->getOperationValue();
        // if no limit, just get commission
        if ($limit->isZero()) {
            return $withdraw->multiply($this->commissionPercent);
        }

        // convert to basic currency currency if not to compare with limit
        if (!$withdraw->sameCurrency($limit)) {
            $withdraw = $this->ratesConverter->convert($withdraw, Money::EUR);
        }

        // if limit grater then operation amount, no commission
        if ($limit->graterThen($withdraw)) {
            return new Money('0', $command->getOperationValue()->getCurrency());
        }

        // if not sub limit from operation amount and get commission with operation currency
        $withdrawCommissionFee = $withdraw->sub($limit);

        $withdrawCommissionFee = $this->ratesConverter->convert(
            $withdrawCommissionFee,
            $command->getOperationValue()->getCurrency()
        );

        return $withdrawCommissionFee->multiply($this->commissionPercent); // 0.003
    }

    /**
     * @param  CommissionFeeCommand $command
     *
     * @return bool
     */
    public function isResponsible(CommissionFeeCommand $command): bool
    {
        return $command->getOperationType()->isWithdraw() && $command->getUserType()->isPrivate();
    }
}