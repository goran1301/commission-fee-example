<?php

declare(strict_types=1);

namespace App\Domain\Exception\ValueObjects;

use App\Domain\Exception\DomainException;
use Throwable;


/**
 * Class UserTypeIsNotConfigured
 *
 * @package App\Domain\Exception\ValueObjects
 */
class UserTypeIsNotConfigured extends DomainException
{
    /**
     * UserTypeIsNotConfigured constructor.
     *
     * @param  int            $type
     * @param  int            $code
     * @param  Throwable|null $previous
     */
    public function __construct(int $type, int $code = 0, Throwable $previous = null)
    {
        parent::__construct("User type $type is not configured", $code, $previous);
    }
}