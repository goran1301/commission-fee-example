<?php

declare(strict_types=1);

namespace App\Domain\Services;

use App\Domain\CommandHandlers\CommissionFeeCommandHandler;
use App\Domain\Commands\CommissionFeeCommand;
use App\Domain\Exception\DomainException;
use App\Domain\ValueObjects\Money;

/**
 * Class CommissionFeeCalculator
 *
 * @package App\Domain\Services
 */
class CommissionFeeCalculator
{
    /**
     * @var CommissionFeeCommandHandler[]
     */
    private iterable $commandHandlers;

    /**
     * CommissionFeeCalculator constructor.
     *
     * @param  CommissionFeeCommandHandler[] $commandHandlers
     */
    public function __construct(CommissionFeeCommandHandler... $commandHandlers)
    {
        $this->commandHandlers = $commandHandlers;
    }

    /**
     * @param  CommissionFeeCommand $command
     *
     * @return Money
     * @throws DomainException
     */
    public function calculate(CommissionFeeCommand $command): Money
    {
        foreach ($this->commandHandlers as $commandHandler) {
            if ($commandHandler->isResponsible($command)) {
                return $commandHandler->handle($command);
            }
        }

        throw new DomainException('No handler for command');
    }
}