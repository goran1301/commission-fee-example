<?php

declare(strict_types=1);

namespace App\Domain\ValueObjects;

/**
 * Class OperationType
 *
 * @package App\Domain\ValueObjects
 */
class OperationType
{
    public const TYPE_WITHDRAW = 0;

    public const TYPE_DEPOSIT = 1;

    /**
     * @var int[]
     */
    private array $types=[
        self::TYPE_WITHDRAW,
        self::TYPE_DEPOSIT,
    ];

    /**
     * @var int
     */
    private int $type;

    /**
     * OperationType constructor.
     *
     * @param  int $type
     */
    public function __construct(int $type)
    {
        $this->type = $type;
    }

    /**
     * @return bool
     */
    public function isWithdraw(): bool
    {
        return self::TYPE_WITHDRAW === $this->type;
    }

    /**
     * @return bool
     */
    public function isDeposit(): bool
    {
        return self::TYPE_DEPOSIT === $this->type;
    }

    /**
     * @return int
     */
    public function toInt(): int
    {
        return $this->type;
    }

    /**
     * @param  OperationType $type
     *
     * @return bool
     */
    public function same(self $type): bool
    {
        return $this->type === $type->type;
    }
}