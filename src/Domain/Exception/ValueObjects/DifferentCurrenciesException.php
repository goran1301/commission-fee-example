<?php

declare(strict_types=1);

namespace App\Domain\Exception\ValueObjects;

use App\Domain\Exception\DomainException;
use Throwable;

/**
 * Class DifferentCurrenciesException
 *
 * @package App\Domain\Exception\ValueObjects
 */
class DifferentCurrenciesException extends DomainException
{
    /**
     * DifferentCurrenciesException constructor.
     *
     * @param  string         $firstCurrency
     * @param  string         $secondCurrency
     * @param  int            $code
     * @param  Throwable|null $previous
     */
    public function __construct(string $firstCurrency, string $secondCurrency, $code = 0, Throwable $previous = null)
    {
        parent::__construct(
            "Can not calculate different currencies: $firstCurrency and $secondCurrency", $code, $previous
        );
    }
}