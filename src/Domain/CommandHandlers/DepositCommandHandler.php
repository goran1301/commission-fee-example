<?php

declare(strict_types=1);

namespace App\Domain\CommandHandlers;

use App\Domain\Commands\CommissionFeeCommand;
use App\Domain\Exception\ValueObjects\CurrencyIsNotConfiguredException;
use App\Domain\ValueObjects\Money;

/**
 * Class DepositCommandHandler
 *
 * @package App\Domain\CommandHandlers
 */
class DepositCommandHandler implements CommissionFeeCommandHandler
{
    /**
     * @var string
     */
    private string $commissionPercent;

    /**
     * DepositCommandHandler constructor.
     *
     * @param  string $commissionPercent
     */
    public function __construct(string $commissionPercent)
    {
        $this->commissionPercent = $commissionPercent;
    }

    /**
     * @param  CommissionFeeCommand $command
     *
     * @return Money
     * @throws CurrencyIsNotConfiguredException
     */
    public function handle(CommissionFeeCommand $command): Money
    {
        return $command->getOperationValue()->multiply($this->commissionPercent); // 0.0003
    }

    /**
     * @param  CommissionFeeCommand $command
     *
     * @return bool
     */
    public function isResponsible(CommissionFeeCommand $command): bool
    {
        return $command->getOperationType()->isDeposit();
    }
}