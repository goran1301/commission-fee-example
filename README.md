# Installation

```
# docker-compose build
```
```
# docker-compose up -d 
```

```
# composer install
```

# Run

```
# docker-compose exec php-fpm php bin/console commission:calculate input.csv
```

or

```
# php bin/console commission:calculate input.csv
```

if not under dockrer

The command takes csv data form input.csv and print commissions. 
Uses actual rates, output is different.

# Testing

Run
```
# composer require --dev symfony/phpunit-bridge
```

```
# ./bin/phpunit
```

The tests provide example rates

```
EUR:USD - 1:1.1497, EUR:JPY - 1:129.53
```

input

```
2014-12-31,4,private,withdraw,1200.00,EUR
2015-01-01,4,private,withdraw,1000.00,EUR
2016-01-05,4,private,withdraw,1000.00,EUR
2016-01-05,1,private,deposit,200.00,EUR
2016-01-06,2,business,withdraw,300.00,EUR
2016-01-06,1,private,withdraw,30000,JPY
2016-01-07,1,private,withdraw,1000.00,EUR
2016-01-07,1,private,withdraw,100.00,USD
2016-01-10,1,private,withdraw,100.00,EUR
2016-01-10,2,business,deposit,10000.00,EUR
2016-01-10,3,private,withdraw,1000.00,EUR
2016-02-15,1,private,withdraw,300.00,EUR
2016-02-19,5,private,withdraw,3000000,JPY
```

checking output

```
0.60
3.00
0.00
0.06
1.50
0
0.70
0.30
0.30
3.00
0.00
0.00
8612
```
# Methodology and patterns

This code was designed according Domain Driven Design methodology.

Domain directory contains abstract application model, which includes also value objects and entities,
 which supposed to be stored in database. Domain also includes non abstract classes,
 which contain important business rules independent of framework and external libraries.
 
All calculations of commission fee are provided by `CommissionFeeCalculator` service.
This service consists of `Command handlers` for different commands according the `Command pattern`.
To make command handlers structured well, it uses the `Linker pattern`.

The `CommissionFeeFactory` is using to compose needed CommissionFeeCalculator.
It makes CommissionFeeCalculator well extendable, because to make a new rule, a programmer needs just initialize it
 in factory and pass as argument to constructor. It also easy to remove some rule - just not to pass it in the 
 calculator's constructor.
 
It's important to disallow to inject a command handler separately of the CommissionFeeCalculator.
A single handler can be wrong for a command, because this business rule could be not actual
 for this case anymore. The calculator's factory is a single place to initialize all actual handlers.
 
 
## Configuration

All commission fee percents, the basic application's currency and link for currencies rates are providing by the 
`.env` configuration file.

The exception is the configuration of supported currencies, which is implemented in `Money` value object.
It made for some reasons:
- The count of numbers after point for any currency is not business parameter to change. 
It mostly technical information for correct calculations with money.
- Encapsulation benefits. Money class is able to validate itself and hide the implementation of money calculations under
simple methods, like `Money::sub`, `Money::div` or `Money::multiply`. All that methods are depend on currencies decimals
 count.
- Approximately, the currencies and their decimals numbers is a constant part of the world. That's why we don't need to
 change them via a config.
- It makes much easy to use `Money` class as value object and implement it to any php ORM later.

