<?php

declare(strict_types=1);

namespace App\Factory;

use App\Domain\Calculations\PrivateWithdrawCommissionFeeFeeLimit;
use App\Domain\CommandHandlers\CommissionFeeCommandHandler;
use App\Domain\CommandHandlers\DepositCommandHandler;
use App\Domain\CommandHandlers\WithdrawBusinessCommissionFeeCommandHandler;
use App\Domain\CommandHandlers\WithdrawCommissionFeeCommandHandler;
use App\Domain\CommandHandlers\WithdrawPrivateCommissionFeeCommandHandler;
use App\Domain\Repositories\PaymentHistoryNotesRepositoryInterface;
use App\Domain\Services\CommissionFeeCalculator;
use App\Domain\Services\RatesConverterInterface;


/**
 * Class CommissionFeeCalculatorFactory
 *
 * @package App\Factory
 */
class CommissionFeeCalculatorFactory
{
    /**
     * @param  RatesConverterInterface                $ratesConverter
     * @param  PaymentHistoryNotesRepositoryInterface $historyNotesRepository
     * @param  PrivateWithdrawCommissionFeeFeeLimit   $limitCalculation
     *
     * @param  string                                 $privateWithdrawPercent
     * @param  string                                 $businessWithdrawPercent
     * @param  string                                 $depositPercent
     *
     * @return CommissionFeeCalculator
     */
    public function make(
        RatesConverterInterface $ratesConverter,
        PaymentHistoryNotesRepositoryInterface $historyNotesRepository,
        PrivateWithdrawCommissionFeeFeeLimit $limitCalculation,
        string $privateWithdrawPercent,
        string $businessWithdrawPercent,
        string $depositPercent
    ): CommissionFeeCalculator
    {
        $depositCommandHandler = $this->depositStrategy($depositPercent);

        $withdrawCommandHandler = $this->withdrawStrategy(
            $ratesConverter,
            $historyNotesRepository,
            $limitCalculation,
            $businessWithdrawPercent,
            $privateWithdrawPercent
        );

        return new CommissionFeeCalculator($depositCommandHandler, $withdrawCommandHandler);
    }

    /**
     * @param  string $depositPercent
     *
     * @return CommissionFeeCommandHandler
     */
    private function depositStrategy(string $depositPercent): CommissionFeeCommandHandler
    {
        return new DepositCommandHandler($depositPercent);
    }

    /**
     * @param  RatesConverterInterface                $ratesConverter
     * @param  PaymentHistoryNotesRepositoryInterface $historyNotesRepository
     * @param  PrivateWithdrawCommissionFeeFeeLimit   $limitCalculation
     * @param  string                                 $businessWithdrawPercent
     * @param  string                                 $privateWithdrawPercent
     *
     * @return CommissionFeeCommandHandler
     */
    private function withdrawStrategy(
        RatesConverterInterface $ratesConverter,
        PaymentHistoryNotesRepositoryInterface $historyNotesRepository,
        PrivateWithdrawCommissionFeeFeeLimit $limitCalculation,
        string $businessWithdrawPercent,
        string $privateWithdrawPercent
    ): CommissionFeeCommandHandler
    {
        return new WithdrawCommissionFeeCommandHandler(
            new WithdrawBusinessCommissionFeeCommandHandler($businessWithdrawPercent),
            new WithdrawPrivateCommissionFeeCommandHandler(
                $ratesConverter,
                $historyNotesRepository,
                $limitCalculation,
                $privateWithdrawPercent
            )
        );
    }
}