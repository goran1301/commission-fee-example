<?php

declare(strict_types=1);

namespace App\Domain\Services;

use App\Domain\ValueObjects\Money;

/**
 * Interface RatesConverterInterface
 *
 * @package App\Domain\Services
 */
interface RatesConverterInterface
{
    /**
     * @param  Money  $money
     * @param  string $currency
     *
     * @return Money
     */
    public function convert(Money $money, string $currency): Money;
}