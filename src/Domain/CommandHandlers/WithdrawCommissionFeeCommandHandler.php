<?php

declare(strict_types=1);

namespace App\Domain\CommandHandlers;

use App\Domain\Commands\CommissionFeeCommand;
use App\Domain\ValueObjects\Money;
use DomainException;

/**
 * Class WithdrawCommissionFeeCommandHandler
 *
 * @package App\Domain\CommandHandlers
 */
class WithdrawCommissionFeeCommandHandler implements CommissionFeeCommandHandler
{
    /**
     * @var CommissionFeeCommandHandler[]
     */
    private iterable $handlers;

    /**
     * WithdrawCommissionFeeCommandHandler constructor.
     *
     * @param  CommissionFeeCommandHandler ...$commandHandlers
     */
    public function __construct(CommissionFeeCommandHandler... $commandHandlers)
    {
        $this->handlers = $commandHandlers;
    }

    /**
     * @param  CommissionFeeCommand $command
     *
     * @return Money
     */
    public function handle(CommissionFeeCommand $command): Money
    {
        foreach ($this->handlers as $handler) {
            if ($handler->isResponsible($command)) {
                return $handler->handle($command);
            }
        }

        throw new DomainException(
            "no withdraw command handlers for command"
        );
    }

    /**
     * @param  CommissionFeeCommand $command
     *
     * @return bool
     */
    public function isResponsible(CommissionFeeCommand $command): bool
    {
        return $command->getOperationType()->isWithdraw();
    }
}