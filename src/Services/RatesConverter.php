<?php

declare(strict_types=1);

namespace App\Services;

use App\Domain\Exception\DomainException;
use App\Domain\Exception\ValueObjects\CurrencyIsNotConfiguredException;
use App\Domain\Services\RatesConverterInterface;
use App\Domain\ValueObjects\CurrencyRate;
use App\Domain\ValueObjects\Money;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

/**
 * Class RatesConverter
 *
 * @package App\Services
 */
class RatesConverter implements RatesConverterInterface
{
    /**
     * @var HttpClientInterface
     */
    private HttpClientInterface $httpClient;

    /**
     * @var string
     */
    private string $currencyServiceLink;

    /**
     * @var array
     */
    private array $rates = [];

    /**
     * RatesConverter constructor.
     *
     * @param  HttpClientInterface $httpClient
     * @param  string              $currencyServiceLink
     */
    public function __construct(HttpClientInterface $httpClient, string $currencyServiceLink)
    {
        $this->httpClient = $httpClient;
        $this->currencyServiceLink = $currencyServiceLink;
    }

    /**
     * @param  Money  $money
     * @param  string $currency
     *
     * @return Money
     * @throws ClientExceptionInterface
     * @throws CurrencyIsNotConfiguredException
     * @throws DecodingExceptionInterface
     * @throws DomainException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function convert(Money $money, string $currency): Money
    {
        if (empty($this->rates)) {
            $this->load();
        }

        if ($money->currencyIs($currency)) {
            return $money;
        }

        if (!in_array($money->getCurrency(), array_keys($this->rates))) {
            throw new DomainException(
                "there is no $currency rate"
            );
        }

        return $money->toCurrency($this->makeRate($money, $currency));
    }

    /**
     * @param  Money  $money
     * @param  string $currency
     *
     * @return CurrencyRate
     */
    private function makeRate(Money $money, string $currency): CurrencyRate
    {
        if ($money->currencyIs(Money::EUR)) {
            return new CurrencyRate(
                $money->getCurrency(),
                $currency,
                $this->rates[$currency],
                true
            );
        }

        return new CurrencyRate(
            $money->getCurrency(),
            $currency,
            $this->rates[$money->getCurrency()]
        );
    }

    /**
     * @throws DomainException
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    private function load(): void
    {
        $response = $this->httpClient->request(
            'GET',
            $this->currencyServiceLink
        );

        if ($response->getStatusCode() !== 200) {
            throw new DomainException('currency rates are unreachable');
        }
        $content = $response->toArray()['rates'];

        foreach ($content as $currency => $rate) {
            $content[$currency] = (string)$rate;
        }
        $content[Money::EUR] = '1';

        $this->rates = $content;
    }
}