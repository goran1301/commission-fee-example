<?php

declare(strict_types=1);

namespace App\Services;

use App\Domain\Commands\CommissionFeeCommand;
use App\Domain\Exception\DomainException;
use App\Domain\Exception\ValueObjects\CurrencyIsNotConfiguredException;
use App\Domain\ValueObjects\Id;
use App\Domain\ValueObjects\Money;
use App\Domain\ValueObjects\OperationType;
use App\Domain\ValueObjects\UserType;
use App\Entity\PaymentHistoryNote;
use DateTime;

/**
 * Class PaymentsImportCSVCommander
 *
 * @package App\Command
 */
class PaymentsImportCSVCommander
{
    /**
     * @var CsvReader
     */
    private CsvReader $reader;

    /**
     * @var string
     */
    private string $basicCurrency;

    /**
     * PaymentsImportCSVCommander constructor.
     *
     * @param  CsvReader $reader
     * @param  string    $basicCurrency
     */
    public function __construct(CsvReader $reader, string $basicCurrency)
    {
        $this->reader = $reader;
        $this->basicCurrency = $basicCurrency;
    }

    /**
     * @param  string $fileName
     *
     * @return iterable
     * @throws CurrencyIsNotConfiguredException|DomainException
     */
    public function buildCommandsFromScv(string $fileName): iterable
    {
        $commands = [];
        foreach ($this->reader->read($fileName) as $data) {
            $commands[] = $this->buildCommand($data);
        }

        return $commands;
    }

    /**
     * @param  array $dataRow
     *
     * @return CommissionFeeCommand
     * @throws CurrencyIsNotConfiguredException
     * @throws DomainException
     */
    public function buildCommand(array $dataRow): CommissionFeeCommand
    {
        $dataRow = array_map(function (string $item) {
            return trim((string) $item);
        }, $dataRow);

        return new CommissionFeeCommand(
            new Money($dataRow[4], $dataRow[5]),
            $this->makeOperationType($dataRow['3']),
            $this->makeUserType($dataRow['2']),
            new Id((int) $dataRow[1]),
            DateTime::createFromFormat('Y-m-d',$dataRow[0])
        );
    }

    /**
     * @param  string $data
     *
     * @return OperationType
     * @throws DomainException
     */
    private function makeOperationType(string $data): OperationType
    {
        switch ($data) {
            case 'withdraw':
                return new OperationType(OperationType::TYPE_WITHDRAW);
            case 'deposit':
                return new OperationType(OperationType::TYPE_DEPOSIT);
            default:
                throw new DomainException("incorrect operation type: $data");
        }
    }

    /**
     * @param  string $data
     *
     * @return UserType
     * @throws DomainException
     */
    private function makeUserType(string $data): UserType
    {
        switch ($data) {
            case 'private':
                return new UserType(UserType::TYPE_PRIVATE);
            case 'business':
                return new UserType(UserType::TYPE_BUSINESS);
            default:
                throw new DomainException("incorrect user type: $data");
        }
    }

    /**
     * @param  CommissionFeeCommand $command
     * @param  Money                $basicCurrencyAmount
     *
     * @return PaymentHistoryNote
     * @throws DomainException
     */
    public function makePaymentHistoryNote(CommissionFeeCommand $command, Money $basicCurrencyAmount): PaymentHistoryNote
    {
        if (!$basicCurrencyAmount->currencyIs($this->basicCurrency)) {
            throw new DomainException("Wrong currency given ({$basicCurrencyAmount->getCurrency()})");
        }

        return new PaymentHistoryNote(
            $command->getOperationValue(),
            $command->getOperationType(),
            $command->getUserIdentifier(),
            $basicCurrencyAmount,
            $command->getDate()
        );
    }
}