<?php

declare(strict_types=1);

namespace App\Domain\ValueObjects;

/**
 * Class CurrencyRate
 *
 * @package App\Domain\ValueObjects
 */
class CurrencyRate
{
    /**
     * @var string
     */
    private string $from;

    /**
     * @var string
     */
    private string $to;

    /**
     * @var string
     */
    private string $value;

    /**
     * @var bool
     */
    private bool $invert;

    /**
     * CurrencyRate constructor.
     *
     * @param  string $from
     * @param  string $to
     * @param  string $value
     * @param  bool   $invert
     */
    public function __construct(string $from, string $to, string $value, bool $invert = false)
    {
        $this->from = $from;
        $this->to = $to;
        $this->value = $value;
        $this->invert = $invert;
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * @return string
     */
    public function getFrom(): string
    {
        return $this->from;
    }

    /**
     * @return string
     */
    public function getTo(): string
    {
        return $this->to;
    }

    /**
     * @return bool
     */
    public function isInvert(): bool
    {
        return $this->invert;
    }
}