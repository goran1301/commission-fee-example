<?php

declare(strict_types=1);

namespace App\Tests\Mock;

use App\Domain\Exception\ValueObjects\CurrencyIsNotConfiguredException;
use App\Domain\Services\RatesConverterInterface;
use App\Domain\ValueObjects\CurrencyRate;
use App\Domain\ValueObjects\Money;
use PHPUnit\Util\Exception;

/**
 * Class RatesConverterToEuroServiceMock
 *
 * @package App\Tests\Mock
 */
class RatesConverterToEuroServiceMock implements RatesConverterInterface
{
    /**
     * @var array
     */
    private array $rates = [
        'USD' => '1.1497',
        'JPY' => '129.53',
        'EUR' => '1'
    ];

    /**
     * @param  Money  $money
     * @param  string $currency
     *
     * @return Money
     * @throws CurrencyIsNotConfiguredException
     */
    public function convert(Money $money, string $currency): Money
    {
        if ($money->currencyIs($currency)) {
            return $money;
        }

        if (!in_array($money->getCurrency(), array_keys($this->rates))) {
            throw new Exception(
                "the $currency rate in RatesConverterToEuroServiceMock is not configured"
            );
        }

        return $money->toCurrency($this->makeRate($money, $currency));
    }

    /**
     * @param  Money  $money
     * @param  string $currency
     *
     * @return CurrencyRate
     */
    private function makeRate(Money $money, string $currency): CurrencyRate
    {
        if ($money->currencyIs(Money::EUR)) {
            return new CurrencyRate(
                $money->getCurrency(),
                $currency,
                $this->rates[$currency],
                true
            );
        }

        return new CurrencyRate(
            $money->getCurrency(),
            $currency,
            $this->rates[$money->getCurrency()]
        );
    }
}