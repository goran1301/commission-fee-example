<?php

declare(strict_types=1);

namespace App\Tests\Domain\Services;

use App\Services\CsvReader;
use App\Services\PaymentsImportCSVCommander;
use App\Domain\Calculations\PrivateWithdrawCommissionFeeFeeLimit;
use App\Domain\Exception\DomainException;
use App\Domain\Exception\ValueObjects\CurrencyIsNotConfiguredException;
use App\Domain\Repositories\PaymentHistoryNotesRepositoryInterface;
use App\Domain\Services\CommissionFeeCalculator;
use App\Domain\Services\RatesConverterInterface;
use App\Domain\ValueObjects\Money;
use App\Factory\CommissionFeeCalculatorFactory;
use App\Repository\InMemoryPaymentHistoryNotesRepository;
use App\Tests\Mock\RatesConverterToEuroServiceMock;
use PHPUnit\Framework\TestCase;

/**
 * Class CommissionFeeCalculatorTest
 *
 * @package App\Tests\Domain\Services
 */
class CommissionFeeCalculatorTest extends TestCase
{
    /**
     *
     * @return array
     */
    private function data(): array
    {
        return [
            ['2014-12-31',4,'private','withdraw','1200.00','EUR', '0.60'],
            ['2015-01-01',4,'private','withdraw','1000.00','EUR', '3.00'],
            ['2016-01-05',4,'private','withdraw','1000.00','EUR', '0.00'],
            ['2016-01-05',1,'private','deposit','200.00','EUR', '0.06'],
            ['2016-01-06',2,'business','withdraw','300.00','EUR', '1.50'],
            ['2016-01-06',1,'private','withdraw','30000','JPY', '0'],
            ['2016-01-07',1,'private','withdraw','1000.00','EUR', '0.70'],
            ['2016-01-07',1,'private','withdraw','100.00','USD', '0.30'],
            ['2016-01-10',1,'private','withdraw','100.00','EUR', '0.30'],
            ['2016-01-10',2,'business','deposit','10000.00','EUR', '3.00'],
            ['2016-01-10',3,'private','withdraw','1000.00','EUR', '0.00'],
            ['2016-02-10',1,'private','withdraw','300.00','EUR', '0.00'],
            ['2016-02-10',5,'private','withdraw','3000000','JPY', '8612'],
        ];
    }

    /**
     * @param  CommissionFeeCalculator                $calculator
     * @param  PaymentHistoryNotesRepositoryInterface $paymentHistoryNotesRepository
     *
     * @param  RatesConverterInterface                $ratesConverter
     *
     * @throws CurrencyIsNotConfiguredException
     * @throws DomainException
     */
    private function procession(
        CommissionFeeCalculator $calculator,
        PaymentHistoryNotesRepositoryInterface $paymentHistoryNotesRepository,
        RatesConverterInterface $ratesConverter
    ): void
    {
        $commander = new PaymentsImportCSVCommander($this->createMock(CsvReader::class), 'EUR');
        foreach ($this->data() as $dataRow) {
            $command = $commander->buildCommand($dataRow);
            $commission = $calculator->calculate($command);
            $this->assertEquals($dataRow[6], (string) $commission, implode(' ', $dataRow));
            $euroValue = $ratesConverter->convert($command->getOperationValue(), Money::EUR);
            $paymentHistoryNotesRepository->save($commander->makePaymentHistoryNote($command, $euroValue));
        }
    }

    /**
     * @throws CurrencyIsNotConfiguredException
     * @throws DomainException
     */
    public function testCalculate()
    {
        $repository = new InMemoryPaymentHistoryNotesRepository();
        $ratesConverter = new RatesConverterToEuroServiceMock();
        $factory = new CommissionFeeCalculatorFactory();

        $commissionFeeCalculator = $factory->make(
            $ratesConverter,
            $repository,
            new PrivateWithdrawCommissionFeeFeeLimit('1000', 3, 'EUR'),
            '0.003',
            '0.005',
            '0.0003'
        );

        $this->procession($commissionFeeCalculator, $repository, $ratesConverter);

    }
}