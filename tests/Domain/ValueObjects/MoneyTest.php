<?php

declare(strict_types=1);

namespace App\Tests\Domain\ValueObjects;

use App\Domain\ValueObjects\Money;
use PHPUnit\Framework\TestCase;

/**
 * Class MoneyTest
 *
 * @package App\Tests\Domain\ValueObjects
 */
class MoneyTest extends TestCase
{
    public function testCreate()
    {
        $hundredEur = new Money('100', Money::EUR);
        $this->assertEquals(10000, $hundredEur->getInteger());
    }
}