<?php

declare(strict_types=1);

namespace App\Domain\Exception;

use Exception;

/**
 * Class DomainException
 *
 * @package App\Domain\Exception
 */
class DomainException extends Exception
{
}