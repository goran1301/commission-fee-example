<?php

declare(strict_types=1);

namespace App\Command;

use App\Domain\Exception\DomainException;
use App\Domain\Repositories\PaymentHistoryNotesRepositoryInterface;
use App\Domain\Services\CommissionFeeCalculator;
use App\Domain\Services\RatesConverterInterface;
use App\Domain\ValueObjects\Money;
use App\Services\PaymentsImportCSVCommander;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Throwable;

/**
 * Class PaymentsImportCommand
 *
 * @package App\Command
 */
class PaymentsImportCommand extends Command
{
    /**
     * @var CommissionFeeCalculator
     */
    private CommissionFeeCalculator $calculator;

    /**
     * @var PaymentHistoryNotesRepositoryInterface
     */
    private PaymentHistoryNotesRepositoryInterface $paymentsRepository;

    /**
     * @var RatesConverterInterface
     */
    private RatesConverterInterface $ratesConverter;

    /**
     * @var PaymentsImportCSVCommander
     */
    private PaymentsImportCSVCommander $commander;

    /**
     * @var string
     */
    private string $baseCurrency;

    /**
     * PaymentsImportCommand constructor.
     *
     * @param  CommissionFeeCalculator                $calculator
     * @param  PaymentHistoryNotesRepositoryInterface $paymentsRepository
     * @param  RatesConverterInterface                $ratesConverter
     * @param  PaymentsImportCSVCommander             $commander
     * @param  string                                 $baseCurrency
     */
    public function __construct(
        CommissionFeeCalculator $calculator,
        PaymentHistoryNotesRepositoryInterface $paymentsRepository,
        RatesConverterInterface $ratesConverter,
        PaymentsImportCSVCommander $commander,
        string $baseCurrency
    ) {
        $this->calculator = $calculator;
        $this->paymentsRepository = $paymentsRepository;
        $this->ratesConverter = $ratesConverter;
        $this->commander = $commander;
        $this->baseCurrency = $baseCurrency;

        parent::__construct();
    }

    /**
     * @var string
     */
    protected static $defaultName = 'commission:calculate';

    /**
     * @param  InputInterface  $input
     * @param  OutputInterface $output
     *
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        try {

            foreach ($this->commander->buildCommandsFromScv($input->getArgument('path')) as $command) {
                $commission = $this->calculator->calculate($command);
                $output->writeln($commission);
                $euroCommandValue = $this->ratesConverter->convert($command->getOperationValue(), $this->baseCurrency);
                $this->paymentsRepository->save($this->commander->makePaymentHistoryNote($command, $euroCommandValue));
            }

            return Command::SUCCESS;
        } catch (DomainException $exception) {
            $output->writeln($exception->getMessage());
        } catch (Throwable $exception) {
            $output->writeln("server error");
        }

        return Command::FAILURE;
    }

    protected function configure()
    {
        $this->setDescription('Calculates commission fees from csv file');
        $this->setHelp('Calculates commission fees from csv file');

        $this->addArgument('path', InputArgument::REQUIRED, 'csv file path');
    }
}