<?php

declare(strict_types=1);

namespace App\Domain\Commands;

use App\Domain\ValueObjects\Id;
use App\Domain\ValueObjects\Money;
use App\Domain\ValueObjects\OperationType;
use App\Domain\ValueObjects\UserType;
use DateTime;

/**
 * Class CommissionFeeCommand
 *
 * @package App\Domain\Commands
 */
class CommissionFeeCommand
{
    /**
     * @var DateTime
     */
    private DateTime $date;

    /**
     * @var Id
     */
    private Id $userIdentifier;

    /**
     * @var UserType
     */
    private UserType $userType;

    /**
     * @var OperationType
     */
    private OperationType $operationType;

    /**
     * @var Money
     */
    private Money $operationValue;

    /**
     * CommissionFeeCommand constructor.
     *
     * @param  Money         $operationValue
     * @param  OperationType $operationType
     * @param  UserType      $userType
     * @param  Id           $userIdentifier
     * @param  DateTime      $date
     */
    public function __construct(
        Money $operationValue,
        OperationType $operationType,
        UserType $userType,
        Id $userIdentifier,
        DateTime $date
    ) {
        $this->operationValue = $operationValue;
        $this->operationType = $operationType;
        $this->userType = $userType;
        $this->userIdentifier = $userIdentifier;
        $this->date = $date;
    }

    /**
     * @return DateTime
     */
    public function getDate(): DateTime
    {
        return $this->date;
    }

    /**
     * @return OperationType
     */
    public function getOperationType(): OperationType
    {
        return $this->operationType;
    }

    /**
     * @return Money
     */
    public function getOperationValue(): Money
    {
        return $this->operationValue;
    }

    /**
     * @return Id
     */
    public function getUserIdentifier(): Id
    {
        return $this->userIdentifier;
    }

    /**
     * @return UserType
     */
    public function getUserType(): UserType
    {
        return $this->userType;
    }

}