<?php

declare(strict_types=1);

namespace App\Domain\Exception\ValueObjects;

use App\Domain\Exception\DomainException;
use Throwable;


/**
 * Class CurrencyIsNotConfiguredException
 *
 * @package App\Domain\Exception\ValueObjects
 */
class CurrencyIsNotConfiguredException extends DomainException
{
    /**
     * CurrencyIsNotConfiguredException constructor.
     *
     * @param  string         $currency
     * @param  int            $code
     * @param  Throwable|null $previous
     */
    public function __construct(string $currency, $code = 0, Throwable $previous = null)
    {
        parent::__construct("Currency $currency is not configured", $code, $previous);
    }
}